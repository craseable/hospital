#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <iostream>
#include <string>

using namespace std;

class Pessoa{
	private:
		string Nome;
	public:
		Pessoa();
		~Pessoa();

		void setNome();
		string getNome();

};

#endif
