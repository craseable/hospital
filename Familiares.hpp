#ifndef FAMILIARES_HPP
#define FAMILIARES_HPP

#include "Pessoa.hpp"
#include <stdlib.h>
#include "Paciente.hpp"

using namespace std;

class Familiares : public Pessoa{
	private:
		string Parentesco;
	public:
		Paciente* paciente = NULL;

		Familiares();
		~Familiares();

		void setParentesco();
		string getParentesco();
};





#endif
