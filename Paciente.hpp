#ifndef PACIENTE_HPP
#define PACIENTE_HPP

#include "Pessoa.hpp"
#include "Medico.hpp"
#include <stdlib.h>

using namespace std;

class Paciente : public Pessoa{
	private:
		string Doenca;
		string Quarto;
		string CPF;
		string RG;
                string Idade;
                string Sexo;
                string Cidade;

		
	public:
		Medico* medico = NULL;
		
		Paciente();
		~Paciente();
	
		
		void setCPF();
		string getCPF();
		
		void setIdade();
		string getIdade();
		
		void setRG();
		string getRG();	
	
		void setSexo();
		string getSexo();

		void setCidade();
		string getCidade();

		void setDoenca();
		string getDoenca();	

		void setQuarto();
		string getQuarto();
};

#endif
