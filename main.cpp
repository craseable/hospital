#include "Paciente.hpp"
#include "Medico.hpp"
#include "Familiares.hpp"

int main(){

	Familiares* familiar = new Familiares();
	Pessoa* NomePaciente = new Pessoa();
	Pessoa* NomeMedico = new Pessoa();
	Pessoa* NomeFamiliar = new Pessoa();
	int opcao = 0;

	do{
	
	cout << "\n\n\tMenu" << endl << "\t1 - Cadastrar novo paciente" << endl << "\t2 - Consultar Paciente" << endl << "\t0 - Sair" << endl << "--->" << endl;
	cin >> opcao;

	switch(opcao){	
		case 0:
			cout << ">> Saindo do programa! <<" << endl;
		break;

		case 1:
			

			cout << "\nDigite o nome do Paciente: " << endl;
			NomePaciente -> setNome();
		
			cout << "Digite a doença de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setDoenca();

			cout << "Digite o CPF de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setCPF();
	
			cout << "Digite a idade de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setIdade();

			cout << "Digite o RG de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setRG();

			cout << "Digite o Sexo de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setSexo();

			cout << "Digite a Cidade de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setCidade();

			cout << "Digite o Quarto de " + NomePaciente -> getNome() + ": " << endl;
			familiar -> paciente -> setQuarto();

			cout << "Digite o nome do Medico de " + NomePaciente -> getNome() + ": " << endl;
			NomeMedico -> setNome();
			cout << "Digite o CRM do Dr. " + NomeMedico -> getNome()+ ":" << endl;
			familiar -> paciente -> medico -> setCRM();
			
			cout << "Digite o nome do acompanhante de " + NomePaciente -> getNome() + ":" << endl;
			NomeFamiliar -> setNome();
			
			cout << "Digite o parentesco de " + NomeFamiliar -> getNome() + " em relação a " + NomePaciente -> getNome() << ":" << endl;
			familiar -> setParentesco();

			break;

			case 2:
				cout << "\n\t\tNome: " + NomePaciente -> getNome() << endl;
				cout << "\t\tIdade: " + familiar -> paciente -> getIdade() << endl;
				cout << "\t\tSexo: " + familiar -> paciente -> getSexo() << endl;
				cout << "\t\tRG: " + familiar -> paciente -> getRG() << endl;
				cout << "\t\tCPF: " + familiar -> paciente -> getCPF() << endl;
				cout << "\t\tCidade: " + familiar -> paciente -> getCidade() << endl;
				cout << "\t\tDoença: " + familiar -> paciente -> getDoenca() << endl;
				cout << "\t\tQuarto: " + familiar -> paciente -> getQuarto() << endl;
				cout << "\t\tMédico Responsável: " + NomeMedico -> getNome() << endl;
				cout << "\t\tCRM: " + familiar -> paciente -> medico -> getCRM() << endl;
				cout << "\t\tAcompanhante: " + NomeFamiliar -> getNome() << endl;
				cout << "\t\tParentesco: " + familiar -> getParentesco() << endl;
			break;
			
			default:
				cout << "Opção Indisponível!" << endl;
			break;
		}					

	}while(opcao!=0);

	return 0;
}
