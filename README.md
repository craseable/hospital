Programa que utiliza conceitos de OO para registro de visitas em um hospital, contendo os Pacientes, Médicos e Familiares Visitantes.

Para compilar: 
make

Para executar:
make run

Para limpar:
make clean
