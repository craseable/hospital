#ifndef MEDICO_HPP
#define MEDICO_HPP

#include "Pessoa.hpp"

using namespace std;

class Medico : public Pessoa{
	private: 
		string CRM;
		string Especialidade;
	public:	
		Medico();
		~Medico();

		void setCRM();
		string getCRM();

		void setEspecialidade();
		string getEspecialidade();

};

#endif
